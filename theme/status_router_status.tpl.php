<?php

/**
 * Theme tempate for the status router status
 *  that will be added to user profiles.
 *
 * @file: status_router_status.tpl.php
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 *
 * AVAILABLE VARIABLES:
 *  $status - the status node object
 *  $author - the author user object
 *  $title - the sanitized status text
 *  $created - the post date
 *  $picture - the author's picture
 *  $name - the author's name
 *  $links - node links for the status node
 */
// Uncomment the following lines to see the variables
//print_r($status);
//print_r($title);
//print_r($created);
//print_r($picture);
//print_r($name);
//print_r($links);
?>
<div class="status" id="status-<?php print $statis->nid ?>">

  <?php print $picture ?>
  <?php print $name ?>
  <?php print $title ?>
  <?php print $created ?>

  <div class="links">

    <?php print $links ?>

  </div>

</div>
