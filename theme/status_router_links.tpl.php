<?php

/**
 * Theme template for the status router links
 *  that are appended to the status router node
 *  add form.
 *
 * @file: status_router_links.tpl.php
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 *
 * AVAILABLE VARIABLES: 
 *  $links - array of links for node types
 *    that status_router can route to.
 */
// Uncomment the following lines to view the variables
//print_r($links);
?>
<div class="status_router_links">
  <ul>

    <?php foreach ($links as $link) { ?>
    <li><?php print $link ?></li>
    <?php } ?>

  </ul>
</div>
