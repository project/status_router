=====================================================
 Status Router
=====================================================
This module adds a status node type, a facebook-style
  status input form, and the ability to route a
  status to a different node type from the status
  input form.

=====================================================
 Included Files
=====================================================
README.txt - this file
status_router.info - the info file
status_router.module - the main module
status_router.admin.inc - the admin function callback
  file
status_router.js.inc - the JavaScript callbacks file
status_router.theme.inc - the theme callbacks file
js/status_router.js - the JavaScript for the module
theme/status_router.css - default stylesheet
theme/status_router_links.tpl.php - theme template
  for the status router links.
theme/status_router_status.tpl.php - theme for the
  status that will be included in user profiles

=====================================================
 Dependencies
=====================================================
This module depends on the ajax and ajax_ui modules
  to help validate the status_router node forms. You
  can download these modules here:

  http://drupal.org/project/ajax

=====================================================
 Contact
=====================================================
Questions or comments? Email me:
  Elliott Foster
  http://codebrews.com
  elliottf@codebrews.com

