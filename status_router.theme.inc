<?php

/**
 * Theme callbacks and preprocess functions for
 *  the status_router module.
 *
 * @file: status_router.theme.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * Implementaiton of module_preprocess_status_router_links()
 */
function status_router_preprocess_status_router_links(&$variables) {
  $links =& $variables['links'];

  foreach ($links as $k => $v) {
    $node_type = node_get_types('type', $v);

    // NOTE: DO NOT change the class or id for these links
    // without also updating the JavaScript used to reload
    // the form
    $links[$k] = l(
      $node_type->name, 
      'node/add/' . str_replace('_', '-', $node_type->type),
      array(
        'attributes' => array(
          'class' => 'status_router_link',
          'id' => $node_type->type,
          'title' => $node_type->description
        )
      )
    );
  }
}

/**
 * Implementation of module_preprocess_status_router_status()
 */
function status_router_preprocess_status_router_status(&$variables) {
  $status =& $variables['status'];
  $author = user_load($status->uid);

  $variables['title'] = check_plain($status->title);
  $variables['created'] = date('g:i A M jS', $status->created);
  $variables['picture'] = theme('user_picture', $author);
  $variables['name'] = theme('username', $author);

  // add the links for the node
  $status->links = module_invoke_all('link', 'node', $status, TRUE);
  // but unset the "N reads" link since it's not relevant
  if (isset($status->links['statistics_counter'])) {
    unset($status->links['statistics_counter']);
  }
  drupal_alter('link', $status->links, $status);
  $variables['links'] = theme('links', $status->links);
}

