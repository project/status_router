<?php

/**
 * This module will create a "status" node type,
 *  a facebook-style status input form, and a method
 *  of routing a status to different node types.
 *
 * @file: status_router.module
 * @author: Elliott Foster
 * @copyright: NeWMBC 2009
 */

/**
 * Implementation of hook_help()
 */
function status_router_help($path, $arg) {
  switch ($path) {
    case 'admin/help#status_router':
    return '<p>' . t('Use this module to allow uses to post a status node type and also route statuses to different node types') . '</p>';
  }
}

/**
 * Implementation of hook_enable()
 */
function status_router_enable() {
  // execute the ajax settings form so we can
  // set the variable since it's pretty complicated
  $fs = array();
  drupal_execute('ajax_ui_admin', $fs);
  $ajax_settings = variable_get('ajax', array());

  $ajax_settings['types']['default']['status_node_form']['enabled'] = TRUE;
  variable_set('ajax', $ajax_settings);
}

/**
 * Implementatoin of hook_disable()
 */
function status_router_disable() {
  $ajax_settings = variable_get('ajax', array());
  unset($ajax_settings['types']['default']['status_node_form']);
  variable_set('ajax', $ajax_settings);
}

/**
 * Implementaiton of hook_perm()
 */
function status_router_perm() {
  return array(
    'create status content',   // allow user to post status node type
    'admin status router',     // allow user to access the settings page
  );
}

/**
 * Implementation of hook_menu()
 */
function status_router_menu() {
  $items = array();

  $items['admin/settings/status_router'] = array(
    'title' => 'Status Rotuer Settings',
    'description' => t("Configure the status router module's behavior"),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('status_router_settings'),
    'access arguments' => array('admin status router'),
    'file' => 'status_router.admin.inc',
  );
  $items['status_router/js'] = array(
    'page callback' => 'status_router_js',
    'page arguments' => array(2,3),
    'access arguments' => array('create status content'),
    'type' => MENU_CALLBACK,
    'file' => 'status_router.js.inc',
  );

  return $items;
}

/**
 * Implementation of hook_theme()
 */
function status_router_theme($existing, $type, $theme, $path) {
  return array(
    'status_router_links' => array(
      'arguments' => array('links' => array()),
      'template' => 'theme/status_router_links',
      'file' => 'status_router.theme.inc',
    ),
    'status_router_status' => array(
      'arguments' => array('status' => NULL),
      'template' => 'theme/status_router_status',
      'file' => 'status_router.theme.inc',
    ),
  );
}

/**
 * Implementation of hook_form_alter()
 */
function status_router_form_alter(&$form, $form_state, $form_id) {
  if (arg(0) == 'status_router' && arg(1) == 'js' && arg(2) == 'get') {
    // fist, pull off the preview button so we don't redirect to
    // the JS page.
    if (isset($form['buttons']['preview'])) {
      unset($form['buttons']['preview']);
    }

    // now add the status router links to the form
    $status_router_nodes = status_router_get_nodes(NULL, TRUE);
    // unset the node type we're getting right now
    if (isset($status_router_nodes[arg(3)])) {
      unset($status_router_nodes[arg(3)]);
    }
    if (sizeof($status_router_nodes) > 0) {
      $form['status_router'] = array(
        '#type' => 'fieldset',
        '#title' => t('Status Router'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
      );
      $form['status_router']['nodes'] = array(
        '#type' => 'markup',
        '#value' => theme('status_router_links', $status_router_nodes),
      );
    }

    // now redirect the user back to their profile page
    $form['#submit'][] = 'status_router_js_redirect';
  }
}

/**
 * Impelmentation of hook_node_info()
 */
function status_router_node_info() {
  return array(
    'status' => array(
      'name' => t('Status'),
      'module' => 'status_router',
      'description' => t("Let the world know what you're up to."),
      'has_title' => TRUE,
      'title_label' => t('What are you doing?'),
      'has_body' => FALSE,
    ),
  );
}

/**
 * Implementation of hook_access()
 */
function status_router_access($op, $node, $account) {
  return user_access('create status content', $account);
}

/**
 * Implementation of hook_form()
 */
function status_router_form(&$node, $form_state) {
  $type = node_get_types('type', $node);

  $form['title'] = array(
    '#type'=> 'textfield',
    '#title' => check_plain($type->title_label),
    '#default_value' => $node->title,
    '#required' => TRUE,
  );

  $status_router_nodes = status_router_get_nodes();
  if (sizeof($status_router_nodes) > 0) {
    $form['status_router'] = array(
      '#type' => 'fieldset',
      '#title' => t('Status Router'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['status_router']['nodes'] = array(
      '#type' => 'markup',
      '#value' => theme('status_router_links', $status_router_nodes),
    );
  }

  return $form;
}

/**
 * Implementation of hook_user()
 */
function status_router_user($op, &$edit, &$account, $category = NULL) {
  global $user;

  switch ($op) {
    case 'view':
      if ($user->uid == $account->uid && user_access('create status content')) {
        $css = drupal_get_path('module', 'status_router') . '/theme/status_router.css';
        if (file_exists(path_to_theme() . '/status_router.css')) {
          $css = path_to_theme() . '/status_router.css';
        }
        drupal_add_css($css);

        $account->content['status_router'] = array(
          '#type' => 'markup',
          '#value' => '<div class="status_router_messages"></div><div class="status_router_wrapper">' . drupal_get_form('status_router_profile_form', $account) . '</div>',
        );
      }
      $account->content['last_status'] = array(
        '#type' => 'markup',
        '#value' => status_router_last_status($account),
      );
      break;
  }
}

/**
 * status_router_profile_form - return the form that will be
 *  added to the user profile data
 */
function status_router_profile_form($form_state, $account) {
  $form = array();

  // add the JavaScript - we need to pass the files that are already
  // included to the script so we can be sure not to include them
  // again if the user grabs another form
  $js = drupal_add_js(NULL);
  $js_includes = array();
  foreach ($js['core'] as $include => $vars) {
    $js_includes[] = $include;
  }
  foreach ($js['module'] as $include => $vars) {
    $js_includes[] = $include;
  }
  drupal_add_js(array('js_includes' => $js_includes), 'setting');
  drupal_add_js(drupal_get_path('module', 'status_router') . '/js/status_router.js');

  $form['status'] = array(
    '#type' => 'textfield',
    '#title' => t('What are you doing'),
    '#required' => TRUE,
  );

  $status_router_nodes = status_router_get_nodes($account);
  if (sizeof($status_router_nodes) > 0) {
    $form['status_router'] = array(
      '#type' => 'fieldset',
      '#title' => t('Status Router'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['status_router']['status_router_links'] = array(
      '#type' => 'markup',
      '#value' => theme('status_router_links', $status_router_nodes),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Share'),
  );

  return $form;  
}

/**
 * status_router_profile_form_submit - submit the new status
 */
function status_router_profile_form_submit($form, &$form_state) {
  global $user;

  // call drupal_execute() to submit the node with the value we
  // included here
  module_load_include('inc', 'node', 'node.pages');
  $status = array('type' => 'status');
  $fs = array();
  $fs['values'] = array(
    'uid' => $user->uid,
    'name' => $user->name,
    'title' => $form_state['values']['status'],
    'op' => t('Save'),
  );

  drupal_execute('status_node_form', $fs, (object)$status);
}

/**
 * status_router_get_nodes - return the list of status router
 *  nodes that the current user can post to.
 *
 * @param: $account - the user to verify
 * @param: $include_status - flag to include the status form too
 * @return: array of node types that the current user can
 *  use status_router to post to.
 */
function status_router_get_nodes($account = NULL, $include_status = FALSE) {
  if ($account = NULL) {
    global $user;
    $account = $user;
  }
  else if (is_numeric($account)) {
    $account = user_load($account);
  }

  $status_router_nodes = variable_get('status_router_nodes', array());
  foreach ($status_router_nodes as $k => $v) {
    if (!node_access('create', $v, $account)) {
      unset($status_router_nodes[$k]);
    }
    $status_router_nodes[$k] = node_get_types('type', $v);
  }
  if ($include_status) {
    $status_router_nodes['status'] = node_get_types('type', 'status');
  }

  return $status_router_nodes;
}

/**
 * status_router_last_status - return the last status
 *  for a given user.
 */
function status_router_last_status($account = NULL) {
  $out = '';

  if (user_access('create status content', $account)) {
    $status = db_result(db_query("SELECT nid FROM {node} WHERE type='status' AND uid=%d ORDER BY created DESC LIMIT 1", $account->uid));
    if ($status) {
      $status = node_load($status);
      $out = theme('status_router_status', $status);
    }
  }

  return $out;
}

/**
 * status_router_js_redirect - redirect the user to their
 *  profile after they submit the status router form.
 */
function status_router_js_redirect($form, &$form_state) {
  global $user;

  $form_state['redirect'] = 'user/' . $user->uid;
}

