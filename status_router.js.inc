<?php

/**
 * JavaScript callbacks for the status_router
 *  module.
 *
 * @file: status_router.js.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * status_router_js - main JavaScript handler for
 *  the module.
 */
function status_router_js($op, $type) {
  switch ($op) {
    case 'get':
      module_load_include('inc', 'node', 'node.pages');
      if (!node_access('create', $type)) {
        return drupal_json(array('status' => FALSE, 'data' => 'Access Denied'));
      }

      $form = node_add($type);
      if ($type == 'status') {
        global $user;
        $form = drupal_get_form('status_router_profile_form', $user);
      }

      $js = drupal_add_js(NULL);
      $data = array('form' => $form, 'js' => $js);

      drupal_json(array('status' => TRUE, 'data' => $data));
      break;
  }
}

