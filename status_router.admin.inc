<?php

/**
 * Admin callbacks for the status_router module.
 *
 * @file: status_router.admin.inc
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

/**
 * status_router_settings - system settings form
 */
function status_router_settings() {
  $form = array();

  $options = array();
  $types = node_get_types();
  foreach ($types as $k => $type) {
    if ($k != 'status') {
      $options[$k] = $type->name;
    }
  }
  $form['status_router_nodes'] = array(
    '#type' => 'select',
    '#title' => t('Status Router Nodes'),
    '#options' => $options,
    '#default_value' => variable_get('status_router_nodes', array()),
    '#description' => t('Select the node types that the status router can post to'),
    '#multiple' => TRUE,
  );
  $form['#submit'][] = 'status_router_settings_submit';

  return system_settings_form($form);
}

/**
 * status_router_settings_submit - additional submit
 *  handler for the system settings form.
 */
function status_router_settings_submit($form, &$form_state) {
  // add/remove the selected form values to the ajax
  // form settings
  $ajax_settings = variable_get('ajax', array());

  // we MUST set the selected node types to use
  // the ajax module so form validation works
  // correctly on the status_router form from
  // user profiles.  We can't un-set values though
  // in case the site admin really does want them
  // to use ajax.
  foreach ($form_state['values']['status_router_nodes'] as $node_type) {
    $ajax_settings['types']['default'][$node_type . '_node_form']['enabled'] = TRUE;
  }
  variable_set('ajax', $ajax_settings);
}

