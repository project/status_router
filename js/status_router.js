
/**
 * JavaScript to change the status_router
 *  form on user profiles.
 *
 * @file: status_router.js
 * @author: Elliott Foster
 * @copyright: NewMBC 2009
 */

Drupal.behaviors.status_router = function (context) {
  $('.status_router_link:not(.status_router_link-processed)', context).addClass('status_router_link-processed').click(function() {
    $('.status_router_messages').addClass('throbbing');
    $('.status_router_wrapper').slideUp('slow');
    status_router_get_form(this.id);
    return false; // prevent navigation
  });
}

/**
 * status_router_get_form - get the correct form
 *  via AJAX and replace the current form.
 * @param: type - the type to get
 */
function status_router_get_form(type) {
  var path = Drupal.settings.basePath + 'status_router/js/get/' + type;
  var orig = $('.status_router_wrapper').html();

  $.getJSON(
    path,
    function(json) {
      if (json.status == true) {
        // load all of the JS files that were needed
        for (var i in json.data.js.core) {
          // don't include files that are already included
          if (!Drupal.settings.js_includes.inArray(i)) {
            // have to make sure the full file loads before
            // moving on so we can be sure all functions are
            // available
            $.ajaxSetup({async: false});
            $.getScript(Drupal.settings.basePath + i);
            $.ajaxSetup({async: true});
          }
        }
        for (var i in json.data.js.module) {
          // don't include files that are already included
          if (!Drupal.settings.js_includes.inArray(i)) {
            // have to make sure the full file loads before
            // moving on so we can be sure all functions are
            // available
            $.ajaxSetup({async: false});
            $.getScript(Drupal.settings.basePath + i);
            $.ajaxSetup({async: true});
          }
        }

        // update all of the settings from whatever they were
        // set to on the AJAX callback page
        for (var x in json.data.js.setting) {
          for (var y in json.data.js.setting[x]) {
            // ahah needs to be able to attach multiples for
            // filefield
            if (y == 'ahah') {
              for (var elem in json.data.js.setting[x][y]) {
                if (typeof Drupal.settings[y] == 'undefined') {
                  Drupal.settings[y] = new Object();
                }
                Drupal.settings[y][elem] = json.data.js.setting[x][y][elem];
              }
            }
            else {
              Drupal.settings[y] = json.data.js.setting[x][y];
            }
          }
        }

        $('.status_router_wrapper').hide();
        $('.status_router_wrapper').html(json.data.form);
        Drupal.attachBehaviors('.status_router_wrapper');
        $('.status_router_messages').removeClass('throbbing');
        $('.status_router_wrapper').slideDown('slow');
      }
      else {
        $('.status_router_wrapper').hide();
        $('.status_router_messages').removeClass('throbbing');
        $('.status_router_wrapper').html(orig);
        $('.status_router_wrapper').fadeIn('slow');
      }
    }
  );
}

/**
 * Helper prototype to return true if the passed
 *  in value is foudn in the array.  Code sourced
 *  from this blog: http://www.guyfromchennai.com/?p=28
 */
Array.prototype.inArray = function (value,caseSensitive) {
  var i;
  for (i=0; i < this.length; i++) {
    // use === to check for Matches. ie., identical (===),
    if (caseSensitive) { //performs match even the string is case sensitive
      if (this[i].toLowerCase() == value.toLowerCase()) {
        return true;
       }
    }
    else {
      if (this[i] == value) {
        return true;
      }
    }
  }
  return false;
}
